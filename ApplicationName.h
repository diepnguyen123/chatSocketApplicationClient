#include <conio.h>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <Windows.h>
#include "windows.h"
#include "Box.h"
using namespace std;

class ApplicationName:public Box
{
private:
	int width;
	int height;

public:
	ApplicationName();
	void setHeight(int height);
	void setWidth(int width);
	void draw();
};