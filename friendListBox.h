#include <conio.h>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <Windows.h>
#include "windows.h"
#include "Point.h"
#include "Box.h"
#include <vector>
#include <algorithm>

using namespace std;

class FriendListBox: public Box
{
private:
	int width;
	int height;
public:
	int getWidth();
	int getHeight();
	void setHeight(int height);
	void setWidth(int width);
	FriendListBox();
	void gotoxy(int x, int y);
	
};