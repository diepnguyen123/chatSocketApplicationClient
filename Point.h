#include <conio.h>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <Windows.h>
#include "windows.h"
using namespace std;

class Point
{
private:
	int x;
	int y;

public:
	
	Point();
	Point(int x, int y)
	{
		this->x = x;
		this->y = y;
	}
	void setPoint(int x, int y);
	int getPointX();
	int getPointY();
};